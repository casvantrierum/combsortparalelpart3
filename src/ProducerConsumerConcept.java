import java.util.*;
import java.util.concurrent.*;

/**
 * Created by konin on 22-6-2016.
 */


class Producer implements Runnable {

    protected BlockingQueue<Integer[]> queue;
    protected int counter;
    protected int groupSize;
    protected int max;
    protected int[] numbers;

    public Producer(BlockingQueue<Integer[]> queue, int[] numbers, int max) {
        this.queue = queue;
        this.counter = 0;
        this.groupSize = max/4;
        this.numbers = numbers;
        this.max = max;
    }

    @Override
    public void run() {
        // keep running
        while (((counter)*groupSize) < max) { // remove hard coded max 1
            try {
                long start = System.nanoTime();
                List<Integer> subList = new ArrayList<Integer>();
                for(int number : numbers){
                    if (number >= (counter*groupSize) && number < ((counter+1)*groupSize)){
                        subList.add(number);
                    }
                }

                Integer[] subArr = new Integer[subList.size()];
                subArr = subList.toArray(subArr);

                queue.put(subArr);

                long end  = System.nanoTime();
                System.out.println("Produced " + counter + "(" + subArr.length +  ") in " + (end - start) + "ms");//+ Arrays.toString(subArr));
                counter++;

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Consumer implements Runnable {

    protected BlockingQueue<Integer[]> subQueue;
    protected BlockingQueue<Integer[]> sortedQueue;
    private int id; // to identify a consumer

    public Consumer(int id, BlockingQueue<Integer[]> subQueue, BlockingQueue<Integer[]> sortedQueue) {
        this.subQueue = subQueue;
        this.sortedQueue = sortedQueue;
        this.id = id;
    }

    @Override
    public void run() {
        Integer[] counterArray;

        Random rnd = new Random();

        // keep running
        while (true) {
            try {
                long start = System.nanoTime();
                counterArray = subQueue.take();
                //System.out.println("Consumer takes ");// + Arrays.toString(counterArray));

                counterArray = sort(counterArray, counterArray.length);
                sortedQueue.put(counterArray);
                long end = System.nanoTime();

                System.out.println("Consumer " + id + " sorted " + counterArray[0] + " in " + (end - start) + "ms");// + Arrays.toString(counterArray));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static Integer[] sort(Integer[] array, int size) {

        int gap = size; // The distance between elements being compared
        boolean swapped = true;
        int i; // Our index

        while (gap > 1 || swapped) {
            if (gap > 1) {
                gap = (int)(gap / 1.3);
            }
            if (gap == 9 || gap == 10) {
                gap = 11;
            }

            i = 0;
            swapped = false;

            while (i + gap < size) {
                if (array[i] > array[i+gap]) {
                    swap(array, i, i+gap);
                    swapped = true;
                }
                i++;
            }
        }
        return array;
    }

    private static void swap(Integer[] list, int i, int j) {
        int temp = list[i];
        list[i] = list[j];
        list[j] = temp;
    }
}

class MergeConsumer implements Runnable {

    protected BlockingQueue<Integer[]> queue;
    private long start;

    public MergeConsumer(long start, BlockingQueue<Integer[]> queue) {
        this.start = start;
        this.queue = queue;
    }

    @Override
    public void run() {
        List<Integer> mergedList = new ArrayList<Integer>();
        Integer[] sortedArr;

        // keep running
        while (true) {
            //System.out.println("MergeComsumer running" + mergedList);
            try {
                sortedArr = queue.take();

                //System.out.println("MergeConsumer consumes: " + Arrays.toString(sortedArr));
                // System.out.println("MergeConsumer consumes: "+ sortedArr.length + "numbers with starting element:" + sortedArr[0]);
                int index = mergedList.size();
                if (index != 0) {
                    while(index != 0 && mergedList.get(index - 1) > sortedArr[0]) {
                        index = index - 1;
                    }
                }
                for (int number : sortedArr) {
                    mergedList.add(index, number);
                    index++;
                }
                System.out.println("new size: " + mergedList.size());
//                System.out.println("mergedList: " + mergedList);
                if (mergedList.size() == 100000) {
                    long end = System.nanoTime();
                    System.out.println("DONE in:\t " + (end -start) + "ms");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class ProducerConsumerConcept {

    public void singleProducerMultipleSlowConsumer(int queueCapacity, int[] numbers, int max) throws InterruptedException {
        // a producer/consumer solution with a BlockingQueue
        // solution: https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/BlockingQueue.html

        BlockingQueue<Integer[]> queueSubLists = new ArrayBlockingQueue<Integer[]>(queueCapacity);
        BlockingQueue<Integer[]> queueSortedLists = new ArrayBlockingQueue<Integer[]>(queueCapacity);

        ExecutorService executorService = Executors.newWorkStealingPool();


        long start = System.nanoTime();

        // the producer and consumer share a blocking queue
        Producer producer1 = new Producer(queueSubLists, numbers, max);
        Consumer consumer1 = new Consumer(1, queueSubLists, queueSortedLists);
        Consumer consumer2 = new Consumer(2, queueSubLists, queueSortedLists);
        Consumer consumer3 = new Consumer(3, queueSubLists, queueSortedLists);
        MergeConsumer mergeConsumer = new MergeConsumer(start, queueSortedLists);

        // fire up producer and consumer
        executorService.submit(producer1);
        executorService.submit(consumer1);
        executorService.submit(consumer2);
        //executorService.submit(consumer3);
        executorService.submit(mergeConsumer);

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.DAYS);
        if (executorService.isTerminated())
            System.out.println(queueSubLists.size());
    }



    // use Xint VM setting to prevent JIT optimizations
    public static void main(String[] args) throws InterruptedException {
        int ammount = 100000;
        int min = 0;
        int max = 100000;

        int[] numbers = new int[ammount];
        Random rand = new Random();
        for (int n = 0; n < ammount; n++) {
            numbers[n] = rand.nextInt(max - min ) + min;
        }

        ProducerConsumerConcept pc = new ProducerConsumerConcept();

        pc.singleProducerMultipleSlowConsumer(2, numbers, max);
    }
}
